package com.gitlab.eclipse.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	public static final String GITLAB_INSTANCE_URL = "gitlab.url";
	public static final String IGNORE_CERTIFICATE_ERRORS = "gitlab.ignoreCertificate";
	public static final String LANGUAGE_SERVER_LOG_LEVEL = "gitlab.languageServer.logLevel";
	public static final String LANGUAGE_SERVER_STREAM_CODE_GENERATIONS = "gitlab.languageServer.streamCodeGenerations";
	public static final String TELEMETRY_ENABLED = "gitlab.telemetry.enabled";
	public static final String TANUKI_ONLY_SHOW_CODE_MININGS = "dev.showCodeMinings";
}
